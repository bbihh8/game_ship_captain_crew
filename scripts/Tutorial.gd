extends Control

func _ready():
	
	var new_y_scale = get_node("/root/global").my_viewport_scale
	var new_x_scale = new_y_scale
	
	if get_node("/root/global").actualScreenRatio < 1.4 :
		new_x_scale = new_y_scale * ((800/600) / get_node("/root/global").actualScreenRatio)
	elif get_node("/root/global").actualScreenRatio < 1.61 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.45)
	elif get_node("/root/global").actualScreenRatio < 1.76 :
		new_x_scale = new_y_scale / (get_node("/root/global").actualScreenRatio/1.54)
	else :
		new_x_scale = new_y_scale
	
	get_node("/root").get_children()[2].set_scale(Vector2(new_x_scale,new_y_scale))
	$StageTimer.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _change_scene():
	$StageTimer.stop()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/LaunchMenu.tscn")

func _on_StageTimer_timeout():
	_change_scene()

func _on_tutorialButton_pressed():
	_change_scene()
